require 'spec_helper'

RSpec.describe 'Chrome Settings' do
  let(:session) { Capybara.current_session }

  let(:expected_path) { File.expand_path '~/downloads' }

  def shadow_root(el)
    root = session.execute_script('return arguments[0].shadowRoot', el)
    node = Capybara::Selenium::Node.new(session.driver, root)

    Capybara::Node::Element.new session, node, nil, nil
  end

  before do
    $VERBOSE = false
    session.visit 'chrome://settings/downloads'
    $VERBOSE = true
  end

  describe 'given proper shadow root traversal' do
    it 'uses the correct download directory' do
      settings = session.find('body settings-ui')
      settings = shadow_root(settings)

      settings = settings.find('#container settings-main')
      settings = shadow_root(settings)

      settings = settings.find('settings-basic-page')
      settings = shadow_root(settings)

      css = [
        '#advancedPage',
        'settings-section[section="downloads"]',
        'settings-downloads-page',
      ].join(' ')
      settings = settings.find(css)
      settings = shadow_root(settings)

      downloads_path = settings.find('.settings-box.first .secondary').text
      toggle = settings.find('.settings-box.block settings-toggle-button')

      expect(toggle[:checked]).to eq nil # this could be a false negative
      expect(downloads_path).to match(/^#{expected_path}/i)
    end
  end

  describe 'given the simpler, but less stable /deep/ selector is used' do
    it 'uses the correct download directory' do
      css = [
        'body',
        'settings-ui',
        '/deep/',
        'settings-main',
        '/deep/',
        'settings-basic-page',
        '/deep/',
        '#advancedPage',
        'settings-section[section="downloads"]',
        'settings-downloads-page',
      ].join(' ')
      settings = session.find(css)

      css = [
        'settings-downloads-page',
        '/deep/',
      ]

      path_css = (css + [
        '.settings-box.first',
        '.secondary',
      ]).join ' '

      toggle_css = (css + [
        '.settings-box.block',
        'settings-toggle-button',
      ]).join ' '

      downloads_path = settings.find(path_css).text
      toggle = settings.find(toggle_css)

      expect(toggle[:checked]).to eq nil # this could be a false negative
      expect(downloads_path).to match(/^#{expected_path}/i)
    end
  end
end
