Capybara Shadow DOM
===================

This project illustrates how to access shadow DOM elements using Capybara.


Running
=======

Clone the repo, then run:

	bundle install
	bundle exec rake


This will run the specs, which is all there is.

There is a very slight chance that your downloads folder might not match
what is expected in the specs.
I tried to keep this as generic as possible,
but there may be a case that is not covered.
I'm happy to make changes to support this,
but it is important to note that the significance is not whether the tests pass,
but whether they are able to retrieve the proper values from the browser.


Exploring
=========

The only relevant file, really, is `spec/chrome_settings_spec.rb`.
